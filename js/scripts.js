jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');



	
	$('.mod_custom-header-main, .mod_custom-header').each(function(){
		$('.slider',this).cycle({
			slides: '.item',
			speed: 600,
			timeout: 5000,
			pager: $('.pager',this),
			prev: $('.prev',this),
			next: $('.next',this),
		});
	});



	$('.mod_custom-contact .mod-head .button a').on('click',function(event){
		event.preventDefault();
		$('body').toggleClass('mod_custom-contact-on');
	});



	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}



	// iCheck
	$('input[type="checkbox"],input[type="radio"]').iCheck();



	// selectBoxIt
	$('select:not([multiple]').selectBoxIt();

	


	// basictable
	$('table.normal').basictable({
		tableWrapper: true,
		breakpoint: 920
	});
	/*
	$('table.normal').each(function(index, el) {
		// větší tabulka - povol zalamování
		if ($(this).find('th').length > 1)
		{
			$(this).basictable({
				tableWrapper: true
			});
		}
		// menší tabulka - nastav breakpoint na malý rozměr
		else
		{
			$(this).basictable({
				tableWrapper: true,
				breakpoint: 120 // css media query
			});
		}
	});
	*/



	/* funkce vykonané na připravenost dokumentu */
	onDocumentReady($);



	/* vyčká, než je dokončena jakakoliv událost */
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();



	/* na opravdovou zmenu velikosti okna - konej */
	window._width = $(window).width();
	$(window).resize(function () {
		waitForFinalEvent(function(){

			// detekce skutečné změny
			if (window._width != $(window).width())
			{
				window._width = $(window).width();
				onWindowResize($);
			}

		}, 500, "afterWindowResize");
	});

});


/**
 * funkce vykonané na připravenost dokumentu
 *
 * @param   {function}  jQ  objekt jQuery
 * 
 * @return
 */
function onDocumentReady(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * funkce vykonané na responzivní chování
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function onWindowResize(jQ)
{
	uniteHeights(jQ);				// vyrovnej výšky elementů
	return;
}


/**
 * Funkce pro sjednocování elementů stránky
 * - na základě třídy (unite-heights)
 * a nastavené ID skupiny elementů (data-ug)
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function uniteHeights(jQ)
{
	// seznam skupin
	var u_gs = [];
	jQ('.unite-heights').each(function(i, el) {
		var c_ug = jQ(this).attr('data-ug');
		if (jQ.inArray( c_ug, u_gs ) == -1)
		{
			u_gs.push(c_ug);
		}
	});

	// projdi všechny skupiny a nastav jim stejné výšky
	if (u_gs.length > 0)
	{
		jQ.each(u_gs, function(i, g_id) {

			var heighest = 0;
			s = '[data-ug="' + g_id + '"]';	// data unite group

			// zresetuj a načti nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).css('height', 'auto');	// reset
				heighest = jQ(this).height() > heighest ? jQ(this).height() : heighest;
			});

			// nastav nejvyšší
			jQ(s).each(function(index, el) {
				jQ(this).height(heighest);
			});
		});
	}
	return;
}